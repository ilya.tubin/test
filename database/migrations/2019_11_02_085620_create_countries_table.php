<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

use Illuminate\Support\Facades\DB;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->timestamps();
            $table->softDeletes();
        });

        DB::table('countries')->insert([
            [
                'name' => 'Россия',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Казахстан',
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'name' => 'Украина',
                'created_at' => date('Y-m-d H:i:s')
            ]
        ]);

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('countries');
    }
}
