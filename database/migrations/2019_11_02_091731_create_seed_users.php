<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

use Illuminate\Support\Facades\DB;

class CreateSeedUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        $names = ['Александр','Алексей','Анатолий','Андрей','Антон','Аркадий','Артём','Артур','Борис','Вадим','Валентин','Валерий','Василий','Виктор','Виталий','Владимир','Владислав','Вячеслав','Георгий','Григорий','Денис','Дмитрий','Евгений','Егор','Иван','Игорь','Илья','Кирилл','Константин','Лев','Леонид','Максим','Марк','Михаил','Никита','Николай','Олег','Павел','Пётр','Роман','Руслан','Сергей','Тимур','Фёдор','Юрий','Ярослав'];
        $surnames = ['Иванов','Смирнов','Кузнецов','Попов','Васильев','Петров','Соколов','Михайлов','Новиков','Федоров','Морозов','Волков','Алексеев','Лебедев','Семенов','Егоров','Павлов','Козлов','Степанов','Николаев','Орлов','Андреев','Макаров','Никитин','Захаров','Зайцев','Соловьев','Борисов','Яковлев','Григорьев','Романов','Воробьев','Сергеев','Кузьмин','Фролов','Александров','Дмитриев'];

        $users = [];
        $roles = [];
        $activations = [];

        for($i = 1; $i <= 100000; $i++) {
              print "Обработка $i / " . 100000 . PHP_EOL;
            $first_name = $names[rand(0, count($names)-1)];
            $last_name = $surnames[rand(0, count($surnames)-1)];
            $email = Str::slug($first_name . '_' . $last_name, '_') . '_' . $i . '@gmail.com';

            $city_and_country = rand(1, 3);

            $users[] = [
                'email'    => $email,
                'password' => md5($email) . rand(1000, 9999),
                'first_name' => $first_name,
                'last_name' => $last_name,
                'country_id' => $city_and_country,
                'city_id' => $city_and_country,
                'created_at' => date('Y-m-d H:i:s'),
            ];

            $roles[] =[
                'user_id' => $i,
                'role_id' => 2,
                'created_at' => date('Y-m-d H:i:s')
            ];

            $activations[] = [
                'user_id' => $i,
                'code' => 'code',
                'completed' => 1,
                'completed_at' => date('Y-m-d H:i:s'),
            ];

            if(count($roles) > 1000) {
                DB::table('users')->insert($users);
                DB::table('role_users')->insert($roles);
                DB::table('activations')->insert($activations);

                $users = [];
                $roles = [];
                $activations = [];
            }

        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {

    }
}
