@extends('layouts.app')

@section('title', trans('menu.users'))

@push('js')
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
    <script src="/theme/js/app/pages/panel/users.js"></script>
@endpush

@section('content')
    <div class="row mb-4">
        <div class="col-md-12">
            <button class="btn btn-sn btn-info" name="add-user">{{ trans('forms.add_user') }}</button>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <table class="table">
                <thead>
                    <th>#</th>
                    <th>{{ trans('table.first_name') }}</th>
                    <th>{{ trans('table.last_name') }}</th>
                    <th>{{ trans('table.country') }}</th>
                    <th>{{ trans('table.city') }}</th>
                    <th>{{ trans('table.role') }}</th>
                    <th>{{ trans('table.created_at') }}</th>
                    <th></th>
                </thead>
            </table>
        </div>
    </div>

    <div class="modal" tabindex="-1" role="dialog" name="user">
      <div class="modal-dialog" role="document">
        <div class="modal-content modal-lg">
          <div class="modal-header">
            <h5 class="modal-title">{{ trans('pages.user_info') }}</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
              <form>
                  <div class="row mb-4">
                      <div class="col-md-6">
                          <lable>{{ trans('forms.first_name') }}</label>
                          <input type="text" name="first_name" class="form-control control-sm"/>
                      </div>
                      <div class="col-md-6">
                          <lable>{{ trans('forms.last_name') }}</label>
                          <input type="text" name="last_name" class="form-control control-sm"/>
                      </div>
                  </div>
                  <div class="row mb-4">
                      <div class="col-md-6">
                          <lable>{{ trans('forms.email') }}</label>
                          <input type="text" name="email" class="form-control control-sm"/>
                      </div>
                      <div class="col-md-6">
                          <lable>{{ trans('forms.role') }}</label>
                            <select class="form-control control-sm" name="role_id">
                            </select>
                      </div>
                  </div>
                  <div class="row mb-4">
                      <div class="col-md-6">
                          <lable>{{ trans('forms.country') }}</label>
                          <select class="form-control control-sm" name="country_id">
                          </select>
                      </div>
                      <div class="col-md-6">
                          <lable>{{ trans('forms.city') }}</label>
                            <select class="form-control control-sm" name="city_id">
                            </select>
                      </div>
                  </div>
                  <div class="row mb-4">
                      <div class="col-md-6">
                          <lable>{{ trans('forms.password') }}</label>
                          <input type="text" name="password" class="form-control control-sm"/>
                      </div>
                      <div class="col-md-6">
                          <lable>{{ trans('forms.password_confirmation') }}</label>
                          <input type="text" name="password_confirmation" class="form-control control-sm"/>
                      </div>
                  </div>
              </form>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-primary" name="save">{{ trans('forms.save') }}</button>
            <button type="button" class="btn btn-secondary" data-dismiss="modal">{{ trans('forms.cancel') }}</button>
          </div>
        </div>
      </div>
    </div>
@overwrite
