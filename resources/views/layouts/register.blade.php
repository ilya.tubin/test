<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name') }} - {{ trans('pages.registration') }}</title>

  <!-- Custom fonts for this template-->
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/theme/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <div class="card o-hidden border-0 shadow-lg my-5">
      <div class="card-body p-0">
        <!-- Nested Row within Card Body -->
        <div class="row">
          <div class="col-lg-5 d-none d-lg-block bg-register-image"></div>
          <div class="col-lg-7">
            <div class="p-5">
              <div class="text-center">
                <h1 class="h4 text-gray-900 mb-4">{{ trans('forms.registration') }}</h1>
              </div>
              <form class="user">
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="text" name="first_name" class="form-control form-control-user" id="exampleFirstName" placeholder="{{ trans('forms.first_name') }}">
                  </div>
                  <div class="col-sm-6">
                    <input type="text" name="last_name" class="form-control form-control-user" id="exampleLastName" placeholder="{{ trans('forms.last_name') }}">
                  </div>
                </div>
                <div class="form-group">
                  <input type="email" name="email" class="form-control form-control-user" id="exampleInputEmail" placeholder="{{ trans('forms.email') }}">
                </div>
                <div class="form-group row">
                  <div class="col-sm-6 mb-3 mb-sm-0">
                    <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ trans('forms.password') }}">
                  </div>
                  <div class="col-sm-6">
                    <input type="password" name="password_confirmation" class="form-control form-control-user" id="exampleRepeatPassword" placeholder="{{ trans('forms.password_confirmation') }}">
                  </div>
                </div>
                <a href="#" class="btn btn-primary btn-user btn-block" name="register">{{ trans('forms.registration') }}</a>
              </form>
              <hr>
              <div class="text-center">
                <a class="small" href="{{ route('login') }}">{{ trans('forms.authentication') }}</a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/theme/vendor/jquery/jquery.min.js"></script>
  <script src="/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="/theme/js/app/env.js"></script>
  <script src="/theme/js/app/api.js"></script>
  <script src="/theme/js/app/helpers.js"></script>
  <script src="/theme/js/app/pages/register.js"></script>

</body>

</html>
