<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  <title>{{ config('app.name') }} - {{ trans('pages.authentication') }}</title>

  <!-- Custom fonts for this template-->
  <link href="/theme/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="/theme/css/sb-admin-2.min.css" rel="stylesheet">

</head>

<body class="bg-gradient-primary">

  <div class="container">

    <!-- Outer Row -->
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <!-- Nested Row within Card Body -->
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block bg-login-image"></div>
              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <h1 class="h4 text-gray-900 mb-4">{{ trans('forms.authentication') }}</h1>
                  </div>
                  <form class="user">
                    <div class="form-group">
                      <input type="email" name="email" class="form-control form-control-user" aria-describedby="emailHelp" placeholder="{{ trans('forms.email') }}">
                    </div>
                    <div class="form-group">
                      <input type="password" name="password" class="form-control form-control-user" id="exampleInputPassword" placeholder="{{ trans('forms.password') }}">
                    </div>
                    <a href="#" class="btn btn-primary btn-user btn-block" name="login">{{ trans('forms.authentication') }}</a>
                  </form>
                  <hr>

                  <div class="text-center">
                    <a class="small" href="{{ route('register') }}">{{ trans('forms.registration') }}</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

      </div>

    </div>

  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="/theme/vendor/jquery/jquery.min.js"></script>
  <script src="/theme/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <script src="/theme/js/app/env.js"></script>
  <script src="/theme/js/app/api.js"></script>
  <script src="/theme/js/app/helpers.js"></script>
  <script src="/theme/js/app/pages/login.js"></script>

</body>

</html>
