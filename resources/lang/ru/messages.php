<?php

return [
    'invalid_login_or_password' => 'Неверный логинн или пароль',
    'user_not_found' => 'Пользовватель не найден',
    'forbidden_for_edit_admin_account' => 'Нельзя редактировать системный аккаунт',
    'access_denied' => 'Доступ запрещен',
];
