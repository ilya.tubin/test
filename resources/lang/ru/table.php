<?php

return [
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'country' => 'Страна',
    'city' => 'Город',
    'role' => 'Роль',
    'created_at' => 'Создано',
];
