<?php

return [
    'authentication' => 'Авторизация',
    'registration' => 'Регистрация',
    'email' => 'E-mail',
    'password' => 'Пароль',
    'password_confirmation' => 'Подтверждение пароля',
    'first_name' => 'Имя',
    'last_name' => 'Фамилия',
    'country' => 'Страна',
    'city' => 'Город',
    'save' => 'Сохранить',
    'cancel' => 'Отмена',
    'role' => 'Роль',
    'add_user' => 'Добавить пользователя',
    'logout' => 'Выход',
];
