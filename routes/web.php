<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

include('api.php');

Route::get('/', function() {
    return redirect('/login');
});

Route::get('login', 'RoutesController@login')->middleware('logged:1')->name('login');
Route::get('register', 'RoutesController@register')->middleware('logged:1')->name('register');

Route::middleware('logged')->group(function() {
    Route::get('logout', 'RoutesController@logout')->name('logout');

    Route::prefix('panel')->namespace('Routes')->group(function() {
        Route::get('/users', 'PanelController@users')->name('panel.users')->middleware('role:admin');
    });
});
