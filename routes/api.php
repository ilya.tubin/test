<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('api')->namespace('Api')->group(function() {
    Route::resource('registrations', 'RegistrationsController');
    Route::resource('sessions', 'SessionsController');

    Route::middleware(['logged', 'role:admin'])->group(function() {
        Route::resource('lists', 'ListsController');
        Route::resource('users', 'UsersController');
    });
});
