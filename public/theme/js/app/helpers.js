function inputs(selector, values) {
    if(!values) {
        let result = {};
        $(selector).find('input, select, textarea').each(function() {
            let name = $(this).attr('name');
            if(!name) {
                return true;
            }
            let value = $(this).attr('type') == 'checkbox' ? ($(this).prop('checked') ? 1 : 0) : $(this).val();
            result[name] = value;
        });
        return result;
    }
    for(var name in values) {
        $(selector).find('[name="' + name + '"]').val(values[name]);
    }
}

function onFormRequestError(form, errors) {
    $(form).find('span.error-text').remove();
    $(form).find('div.request-error').remove();
    if(typeof errors == 'object') {
        for(var name in errors) {
            let input = $(form).find('[name="' + name + '"]');
            input.parent().append('<span class="error-text small pl-3 text-danger">' + errors[name].shift() + '</span>');
        }
    }
    if(typeof errors == 'string') {
        $(form).prepend('<div class="alert alert-warning request-error">' + errors + '</div>');
    }

}

function notify(message, type) {
  type = type || 'danger';
  $.notify({
      message: message
  },{
      type: type
  });
}
