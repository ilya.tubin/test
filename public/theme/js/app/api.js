function request(method, url, data) {
    var that = this;
    return new Promise(function(resolve, reject) {
        $.ajax( {
                method: method,
          url: url,
          data: data,
                cache: false,
          success: function(json) {
                if(json['errors']) {
                    return reject(json['errors']);
                }
                if(json['code'] && json['code'] == 200) {
                    return resolve(json['data']);
                }
                if(json['code'] && json['code'] == 302) {
                    window.location = json['data']['url'];
                    return resolve({});
                }
                if(json['code']) {
                    return reject(json['text']);
                }
                return reject('не удалось выполнить запрос', json);
          },
          dataType: 'json'
        } ).fail(function(xhr, error) {
                if(xhr.responseJSON && xhr.responseJSON['errors']) {
                    return reject(xhr.responseJSON['errors']);
                } else if(xhr.responseJSON && xhr.responseJSON['exception']) {
                    return reject(xhr.responseJSON['message']);
                }
                return reject(error)
        }).always(function() {
        });
    });
}

function post(url, params) {
    return request('POST', url, params);
}

function get(url, params) {
    return request('GET', url, params);
}

function update(url, params) {
    params = params || {};
    params['_method'] = 'patch';
    return request('POST', url, params);
}

function remove(url, params) {
    params = params || {};
    params['_method'] = 'delete';
    return request('POST', url, params);
}
