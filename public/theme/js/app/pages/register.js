$(document).ready(function() {
    $(document).on('click', '[name="register"]', onClickRegister);
});

function onClickRegister(e) {
    let form = $(e.currentTarget).parents('form');
    let data = inputs(form);
    post(env.api.url + 'registrations', data).then(onRegistrationSuccess).catch(onFormRequestError.bind(this, form));
    return false;
}

function onRegistrationSuccess(data) {
    console.log(data);
}
