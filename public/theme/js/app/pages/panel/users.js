$(document).ready(function() {
    filterUsersTable();

    $(document).on('click', '.edit', onClickEditUser);
    $(document).on('click', '.remove', onClickRemoveUser);
    $(document).on('click', '[name="save"]', onClickSaveUser);
    $(document).on('click', '[name="add-user"]', onClickAddUser);
});

function modal() {
    return $('.modal[name="user"]');
}

function reloadTable() {
    $('table').DataTable().ajax.reload();
}

function filterUsersTable() {
    $('table').DataTable( {
        searching: false,
        processing: true,
        serverSide: true,
        destroy: true,
        autoWidth: false,
        ordering: false,
        language: {
            "url": "//cdn.datatables.net/plug-ins/1.10.19/i18n/Russian.json"
        },
        ajax: {
            url: env.api.url + 'users',
        },
        columns: [
            {data: 'id'},
            {data: 'first_name'},
            {data: 'last_name'},
            {data: 'country.name'},
            {data: 'city.name'},
            {data: 'role.name'},
            {data: 'created_at'},
            {data: 'id'},
        ],
        columnDefs: [ {
                targets: 7,
                render: function(data, type, row) {
                    return `
                      <a href="#" class="edit fa fa-edit mr-2 text-info" data-id="${row.id}"></a>
                      <a href="#" class="remove fa fa-trash text-danger" data-id="${row.id}"></a>
                    `;
                }
            }
        ]
    } );
}

function onClickEditUser(e) {
    let user_id = $(e.currentTarget).attr('data-id');
    get(env.api.url + 'lists').then(result => {
        initLists(result);
        return get(env.api.url + 'users/' + user_id);
    }).then(onReadUserSuccess).catch(onReadUserFail);

    return false;
}

function initLists(data) {
    modal().find('[name="role_id"]').html(data.roles.map(item => {
        return '<option value="' + item.id + '">' + item.name + '</option>';
    }).join(''))
    modal().find('[name="country_id"]').html(data.countries.map(item => {
        return '<option value="' + item.id + '">' + item.name + '</option>';
    }).join(''))
    modal().find('[name="city_id"]').html(data.cities.map(item => {
        return '<option value="' + item.id + '">' + item.name + '</option>';
    }).join(''))
}

function openNewModal() {
    modal().attr('data-id', '');
    modal().find('input, select, txtarea').val('');
    modal().modal('show');
    onFormRequestError(modal().find('form'));
}

function onReadListsSuccess(data) {
    console.log(data);
}

function onReadUserSuccess(data) {
    openNewModal();
    modal().attr('data-id', data.id);
    inputs(modal().find('form'), data);
}

function onReadUserFail(error) {
    notify(error);
}

function onClickSaveUser() {
    let data = inputs(modal().find('form'));
    if(modal().attr('data-id')) {
        update(env.api.url + 'users/' + modal().attr('data-id'), data).then(onUserSaveSuccess).catch(onFormRequestError.bind(this, modal().find('form')));
    } else {
        post(env.api.url + 'users', data).then(onUserSaveSuccess).catch(onFormRequestError.bind(this, modal().find('form')));
    }
}

function onUserSaveSuccess(data) {
    modal().modal('hide');
    reloadTable();
}

function onClickAddUser() {
    get(env.api.url + 'lists').then(result => {
        initLists(result);
        openNewModal();
    });
}

function onClickRemoveUser(e) {
    if(!confirm('Действительно удалить акаунт?')) {
        return false;
    }
    let user_id = $(e.currentTarget).attr('data-id');

    remove(env.api.url + 'users/' + user_id).then(result => {
        reloadTable();
    }).catch(error => {
        notify(error);
    });

    return false;
}
