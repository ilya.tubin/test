$(document).ready(function() {
    $(document).on('click', '[name="login"]', onClickLogin);
});

function onClickLogin(e) {
    let form = $(e.currentTarget).parents('form');
    let data = inputs(form);
    post(env.api.url + 'sessions', data).then(onLoginSuccess).catch(onFormRequestError.bind(this, form));
    return false;
}

function onLoginSuccess(data) {
    console.log(data);
}
