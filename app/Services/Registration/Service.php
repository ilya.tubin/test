<?php

namespace App\Services\Registration;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use Cartalyst\Sentinel\Laravel\Facades\Activation;
use App\Models\User;
use App\Models\User\Activation as UserActivation;

class Service
{

    public function create($request, $role_slug) {
        $user = Sentinel::registerAndActivate(array(
            'email'    => $request->get('email'),
            'password' => $request->get('password'),
            'first_name' => $request->get('first_name'),
            'last_name' => $request->get('last_name'),
        ));
        $role = Sentinel::findRoleBySlug($role_slug);
        $role->users()->attach($user);

        $user = User::find($user->id);

        return $user;
    }

    public function updatePassword($user_id, $password) {
        return Sentinel::update(Sentinel::findById($user_id), [
            'password' => $password,
        ]);
    }
}
