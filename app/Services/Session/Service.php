<?php

namespace App\Services\Session;

use Cartalyst\Sentinel\Laravel\Facades\Sentinel;
use App\Models\User;

class Service {

    private $user = null;

    public function create($login, $password = null) {
        try {
            if($login instanceof User) {
                Sentinel::login(Sentinel::findById($login->id));
                return true;
            }
            return Sentinel::authenticateAndRemember([
                'email' => $login,
                'password' => $password
            ]) ? $this->user() : false;
        } catch(\Exception $e) {
            return false;
        }
    }

    public function user() {
        if(!$this->user) {
            if(!Sentinel::getUser()) {
                return null;
            }
            $this->user = User::find(Sentinel::getUser()->id);
        }
        return $this->user;
    }

}
