<?php

use App\Services\Session\Service as SessionService;
use \Carbon\Carbon;

function user() {
    return app(SessionService::class)->user();
}
