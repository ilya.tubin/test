<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Registration\Service as RegistrationService;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(RegistrationService::class, function ($app) {
            return new RegistrationService();
        });
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
