<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class Logged
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $invert = null)
    {
        if($invert) {
            if(user()) {
                return redirect(route('panel.users'));
            }
            return $next($request);
        }
        if(!user()) {
            return redirect(route('login'));
        }

        return $next($request);
    }
}
