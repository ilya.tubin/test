<?php

namespace App\Http\Requests;

use \App\Models\Role;

class User extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $user_id = request()->route('user');


        $rules = [
            'first_name' => 'required',
            'last_name' => 'required',
            'email' => 'required|email|unique:users,email' . (empty($user_id) ? '' : ',' . $user_id),
            'password' => (!empty($user_id) ? 'nullable' : 'required'). '|confirmed',
            'city_id' => 'nullable|exists:cities,id',
            'countries' => 'nullable|exists:countries,id',
            'role_id' => 'required|exists:roles,id',

        ];

        return $rules;
    }
}
