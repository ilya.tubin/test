<?php

namespace App\Http\Requests;

use \App\Models\Role;

class Session extends Request
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ];

        return $rules;
    }
}
