<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Services\Registration\Service as RegistrationService;
use \App\Http\Requests\Registration as RegistrationRequest;

class RegistrationsController extends Controller
{
    public function store(RegistrationRequest $request) {
        return $this->catch(function() use($request) {
            $user = app(RegistrationService::class)->create($request, 'client');
            $user->login();
            return $this->redirect(route('panel.users'));
        }, true);
    }
}
