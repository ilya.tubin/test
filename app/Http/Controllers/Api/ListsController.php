<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\Role;
use App\Models\Country;
use App\Models\City;

class ListsController extends Controller
{

    public function index() {
        return $this->ok([
          'roles' => Role::get(),
          'countries' => Country::get(),
          'cities' => City::get(),
        ]);
    }
}
