<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use \App\Http\Requests\Session as SessionRequest;
use App\Services\Session\Service as SessionService;

class SessionsController extends Controller
{
    public function store(SessionRequest $request) {
        $user = app(SessionService::class)->create($request->get('email'), $request->get('password'));
        if(!$user) {
            return $this->fail(trans('messages.invalid_login_or_password'));
        }
        return $this->redirect(route('panel.users'));
    }
}
