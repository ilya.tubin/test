<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Role;
use App\Models\User\Role as UserRole;
use App\Http\Requests\User as UserRequest;
use App\Services\Registration\Service as RegistrationService;

class UsersController extends Controller
{
    public function index() {
        $list = User::orderBy('id', 'asc');

        return $this->table($list, [
            'id',
            'email',
            'first_name',
            'last_name',
            ['role.id' => 'role.id'],
            ['role.name' => 'role.name'],
            ['city.name' => 'city.name'],
            ['country.name' => 'country.name'],
            'created_at',
        ]);
    }

    public function store(UserRequest $request) {
        $request->sanitize();
        return $this->catch(function() use($request) {

            $user = app(RegistrationService::class)->create($request, Role::find($request->get('role_id'))->slug);

            $user->update($request->only(
                'country_id', 'city_id'
            ));

            return $this->ok();
        });
    }

    public function show($id) {
        if($id == 1) {
            return $this->fail(trans('messages.forbidden_for_edit_admin_account'));
        }
        $user = User::selectRaw('id, first_name, last_name, email, city_id, country_id')->find($id);
        if(!$user) {
            return $this->fail(trans('messages.user_not_found'));
        }
        $user->role_id = $user->role->id;
        return $this->ok($user);
    }

    public function update(UserRequest $request, $id) {
        $request->sanitize();
        return $this->catch(function() use($request, $id) {
            $user = User::findOrFail($id);

            $user->update($request->only(
                'first_name', 'last_name', 'email', 'country_id', 'city_id'
            ));

            if($user->role->id != $request->get('role_id')) {
                UserRole::where('user_id', $user->id)->delete();
                UserRole::insert([
                    'user_id' => $user->id,
                    'role_id' => $request->get('role_id'),
                ]);
            }

            if($request->filled('password')) {
                app(RegistrationService::class)->updatePassword($user->id, $request->get('password'));
            }

            return $this->ok();
        }, true);

    }

    public function destroy($id) {
        if($id == 1) {
            return $this->fail(trans('messages.forbidden_for_edit_admin_account'));
        }
        User::findOrFail($id)->delete();
        return $this->ok();
    }
}
