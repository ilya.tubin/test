<?php

namespace App\Http\Controllers\Routes;

use App\Http\Controllers\Controller;

class PanelController extends Controller
{
    public function users() {
        return $this->view('panel.users');
    }
}
