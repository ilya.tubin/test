<?php

namespace App\Http\Controllers;

class RoutesController extends Controller
{
    public function index() {
        return redirect(route('profiles'));
    }

    public function login() {
        return $this->view('login');
    }

    public function register() {
        return $this->view('register');
    }

    public function logout() {
        user()->logout();
        return redirect(route('login'));
    }
}
