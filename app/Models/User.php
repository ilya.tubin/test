<?php

namespace App\Models;

use App\Models\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\User\Role as UserRole;
use Cartalyst\Sentinel\Laravel\Facades\Sentinel;


class User extends Model
{
    use SoftDeletes;

    protected $fillable = ['first_name', 'last_name', 'email', 'country_id', 'city_id'];

    public function role() {
        return $this->hasOneThrough(Role::class, UserRole::class, 'user_id', 'id', 'id', 'role_id');
    }

    public function getFullnameAttribute() {
        return $this->first_name . ' ' . $this->last_name;
    }

    public function country() {
        return $this->belongsTo(Country::class)->selectRaw('id, name');
    }

    public function city() {
        return $this->belongsTo(City::class)->selectRaw('id, name');
    }

    public function login() {
        return Sentinel::login(Sentinel::findById($this->id));
    }

    public function logout() {
        Sentinel::logout();
        return true;
    }

    public function isAdmin() {
        return $this->role->slug == 'admin';
    }

    public function isClient() {
        return $this->getRole()->slug == 'client';
    }
}
