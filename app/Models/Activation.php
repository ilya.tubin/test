<?php

namespace App\Models;

use App\Models\Model;

class Activation extends Model
{
    protected $fillable = ['user_id', 'code', 'completed', 'completed_at', 'created_at'];
}
