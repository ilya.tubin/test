<?php

namespace App\Models;

use App\Models\Model;

class Role extends Model
{
    public static function findBySlug($slug) {
        return self::where('slug', $slug)->first();
    }
}
